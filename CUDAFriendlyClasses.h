// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

#ifndef CALORECGPU_CUDAFRIENDLYCLASSES_H
#define CALORECGPU_CUDAFRIENDLYCLASSES_H

#include <cstddef>
#include <iostream>
#include <limits>


constexpr int NMaxNeighbours = 26;
constexpr int NCaloCells = 187652;
//Max index will be 0x0002 DD03
constexpr int NMaxClusters = 0x10000ULL;
//So it all fits in an int16_t

constexpr int NMaxPairs = NMaxNeighbours * NCaloCells;

constexpr int NumGainStates = 4;
//This is the number of different gain states a cell can have.
//(For the noise calculation.)

//Migrated from macro defines to constexpr, for type safety.

using tag_type = unsigned long long int;
//uint64_t is unsigned long long int in CUDA.


namespace Tags
{

  constexpr tag_type ValidPropagationMark = 0x8000000000000000ULL;
  //0x 8000 0000 0000 0000

  constexpr tag_type GrowTag = ValidPropagationMark;

  constexpr tag_type TerminalTag = 0x1000000000000000ULL;
  //0x 1000 0000 0000 0000

  constexpr tag_type InvalidTag = 0x0001000000000000ULL;
  //0x 0000 0000 0000 0000

  inline constexpr bool is_invalid(const tag_type tag)
  {
    return tag == InvalidTag;
  }

  inline constexpr bool is_valid(const tag_type tag)
  {
    return !is_invalid(tag);
  }

  inline constexpr bool is_non_assigned_terminal(const tag_type tag)
  {
    return tag == TerminalTag;
  }
  
  inline constexpr bool is_assignable_terminal(const tag_type tag)
  {
    return tag >= TerminalTag && tag < GrowTag;
  }

  inline constexpr bool is_assigned_terminal(const tag_type tag)
  {
    return tag > TerminalTag && tag < GrowTag;
  }
  
  inline constexpr bool is_non_assigned_growing(const tag_type tag)
  {
    return tag == GrowTag;
  }

  inline constexpr bool is_growing_or_seed(const tag_type tag)
  {
    return tag >= GrowTag;
  }

  inline constexpr bool is_part_of_cluster(const tag_type tag)
  {
    return tag > GrowTag;
  }

  inline constexpr tag_type make_seed_tag(const int32_t snr_as_int, const int32_t seed_cell_index, const int32_t cluster_index)
  //This works because the numbers will always be non-negative when we make a signature.
  {
    constexpr uint32_t two_byte_mask = 0xFFFFU;
    //By design, the numbers should be clamped to acceptable values.
    //However, no harm in preventing explicit clobbering?
    //Might aid in optimizations, upon naive check on Godbolt/CUDA,
    //due to the existance of "bfi" instructions that do bit-wise insertion.

    const uint32_t index = uint32_t(cluster_index & two_byte_mask);

    tag_type ret = two_byte_mask;


    ret = (ret << 31) | snr_as_int;

    ret = (ret << 16) | index;

    return ret | ValidPropagationMark;
  }

  constexpr tag_type tag_counter_mask = 0x7FFF800000000000ULL;
  //0x FFFF 0000 0000 0000

  constexpr tag_type tag_snr_mask = 0x00007FFFFFFF0000ULL;
  //0x 0000 7FFF FFFF 0000

  constexpr tag_type tag_index_mask = 0xFFFFULL;

  constexpr tag_type tag_propagation_delta = 0x0000800000000000ULL;
  //0x 0000 8000 0000 0000

  inline constexpr int32_t get_snr_from_tag(const tag_type tag)
  {
    const tag_type masked = tag & tag_snr_mask;

    const uint32_t ret = (masked >> 16);

    return ret;
  }

  inline constexpr int32_t get_index_from_tag(const tag_type tag)
  {
    const uint32_t ret = tag & tag_index_mask;

    return ret;
  }

  inline constexpr int32_t get_counter_from_tag(const tag_type tag)
  {
    const tag_type masked = tag & tag_counter_mask;

    const uint32_t ret = (masked >> 47);

    return ret;

  }

  inline constexpr tag_type clear_counter(const tag_type tag)
  {
    return tag & (~tag_counter_mask);
  }

  inline constexpr tag_type clear_everything_but_counter(const tag_type tag)
  {
    return tag & tag_counter_mask;
  }

  inline constexpr tag_type set_for_propagation(const tag_type tag)
  {
    return tag - tag_propagation_delta;
  }

  inline constexpr tag_type set_for_terminal_propagation(const tag_type tag)
  {
    return tag - tag_propagation_delta;
  }


  inline constexpr tag_type set_for_terminal_propagation2(const tag_type tag)
  {
    return (tag - tag_propagation_delta) & (~ValidPropagationMark);
  }
  inline constexpr tag_type update_non_terminal_tag(const tag_type old_tag, const tag_type new_tag)
  {
    return clear_everything_but_counter(old_tag) | new_tag;
  }

  inline constexpr tag_type update_terminal_tag(const tag_type old_tag, const tag_type new_tag)
  {
    return clear_everything_but_counter(old_tag) | new_tag; 
  }
  
  inline constexpr tag_type update_terminal_tag2(const tag_type old_tag, const tag_type new_tag)
  {
    return clear_everything_but_counter(old_tag) | (new_tag & (~ValidPropagationMark)); 
  }

  inline constexpr tag_type terminal_to_seed_tag(const tag_type old_tag)
  {
    return old_tag | ValidPropagationMark;
  }

  /*
  inline constexpr tag_type set_for_neighbour_propagation(const tag_type tag)
  {
    const uint32_t count = get_counter_from_tag(tag) - 1;
    const uint32_t index = get_index_from_tag(tag);
    const uint32_t snr = get_snr_from_tag(tag);
    tag_type ret = count;
    ret = (ret << 31) | snr; //MSB of snr is 0
    ret = (ret << 16) | index;

    //So: 0 ... 16 bit counter ... 31 bit SNR ... 16 bit index

    return ret;
  }


  inline constexpr int32_t get_index_from_terminal_tag(const tag_type tag)
  {
    return tag & 0x0000FFFFU;
  }

  inline constexpr int32_t get_counter_from_terminal_tag(const tag_type tag)
  {
    uint32_t ret = (tag & 0x7FFF800000000000ULL) >> 47;
    //0x 7FFF 8000 0000 0000
    return ret;
  }

  inline constexpr int32_t clear_everything_but_counter_from_terminal_tag(const tag_type tag)
  {
    return tag & 0x7FFF800000000000ULL;
  }

  inline constexpr int32_t get_snr_from_terminal_tag(const tag_type tag)
  {
    uint32_t ret = (tag & 0x00007FFFFFFF0000ULL) >> 16;
    //0x 0000 7FFF FFFF 0000
    return ret;
  }

  inline constexpr tag_type update_non_terminal_tag(const tag_type old_tag, const tag_type new_tag)
  {
    return clear_everything_but_counter(old_tag) | new_tag;
  }

  inline constexpr tag_type update_terminal_tag(const tag_type old_tag, const tag_type new_tag)
  {
    const uint32_t index = get_index_from_tag(new_tag);
    const uint32_t snr = get_snr_from_tag(new_tag);
    tag_type ret = snr;
    ret = (ret << 16) | index;
    return clear_everything_but_counter_from_terminal_tag(old_tag) | ret;
  }

  inline constexpr tag_type terminal_to_seed_tag(const tag_type old_tag)
  {
    tag_type ret = get_snr_from_terminal_tag(old_tag);


    const uint32_t index = get_index_from_terminal_tag(old_tag);

    ret = (ret << 16) | index;

    ret = (ret << 16);

    return ret | ValidPropagationMark;

  }
  */

}


//The following definitions are used to signal certain cells through their energy
//as being ineligible for being seeds, or for being part of a cluster,
//if the cells are being cut according to time (option m_seedCutsInT)

namespace EnergyManip
{
  template <class Base = float, class Exp = int>
  inline constexpr Base compile_time_pow2(const Exp exp)
  {
    Base ret = 1;
    if (exp < 0)
      {
        for (Exp i = 0; i < -exp; ++i)
          {
            ret /= Base(2);
          }
      }
    else
      {
        for (Exp i = 0; i < exp; ++i)
          {
            ret *= Base(2);
          }
      }
    return ret;
  }
  //Though we could possibly bit-hack stuff due to IEEE-754 reliance elsewhere,
  //it's not valid and type-safe C++...
  //Since it's compile-time, this being a trifle slower is meaningless.

  constexpr int invalid_seed_min_exponent = 64;
  constexpr int invalid_seed_factor_exponent = 96;

  constexpr float invalid_seed_min = compile_time_pow2(invalid_seed_min_exponent);
  constexpr float invalid_seed_factor = compile_time_pow2(invalid_seed_factor_exponent);

  constexpr float invalid_for_all_value = std::numeric_limits<float>::max();

  //Cheap trick to allow these functions to be called from a CUDA context
  //without actually placing __host__ __device__ decorations
  //that would be invalid in C++ and cause different signature shenanigans:
  //given that constexpr functions can be called from device code
  //with exprt-relaxed-constexpr,
  //we shall declare them as such even if they are not used in such a way.

  inline constexpr bool is_ineligible_for_any(const float energy)
  {
    return (energy == invalid_for_all_value);
  }

  inline constexpr bool is_ineligible_for_seed(const float energy)
  {
    using namespace std;
    return ( abs(energy) > invalid_seed_min ) && !is_ineligible_for_any(energy);
  }


  inline constexpr bool is_valid_cell(const float energy)
  {
    using namespace std;
    return abs(energy) < invalid_seed_min;
  }

  inline constexpr float correct_energy(const float energy)
  {
    if (is_ineligible_for_seed(energy))
      {
        return energy / invalid_seed_factor;
      }
    else
      {
        return energy;
      }
  }

  inline constexpr void mark_ineligible_for_seed(float & energy)
  {
    energy *= invalid_seed_factor;
  }

  inline constexpr void mark_ineligible_for_all(float & energy)
  {
    energy = invalid_for_all_value;
  }

  //Note: the fact that we use abs(energy) to classify this validity
  //      does not change the results when we use the signed energy
  //      for the cluster growing in itself: as energy < 0 means
  //      the cells will be invalid, they will continue being invalid too...

}


struct CellNoiseArr
{
  float noise[NumGainStates][NCaloCells];
};

enum class gain_type : unsigned char
{
  TileLowLow = 0, TileLowHigh = 1, TileHighLow = 2, TileHighHigh = 3,
  LArLow = 0, LArMedium = 1, LArHigh = 2
};

struct CellInfoArr
{
  float energy[NCaloCells];
  unsigned char gain[NCaloCells];
};


struct GeometryArr
{
  int    caloSample[NCaloCells];
  float  x[NCaloCells];
  float  y[NCaloCells];
  float  z[NCaloCells];
  float  eta[NCaloCells];
  float  phi[NCaloCells];
  int nNeighbours[NCaloCells];               //Number of neighbours
  int neighbours[NCaloCells][NMaxNeighbours]; //NMaxNeighbours = 26
};

struct CellStateArr
{
  tag_type clusterTag[NCaloCells]; //cluster tag
};

struct PairsArr
{
  int number;
  int cellID[NMaxPairs];
  //This is guaranteed growing or seed cell
  int neighbourID[NMaxPairs];
  //This is any cell
};

struct ClusterInfoArr
{
  int number;
  //tag_type clusterTag[NMaxClusters];
  //int clusterSize[NMaxClusters];
  float clusterEnergy[NMaxClusters];
  float clusterEt[NMaxClusters];
  //Also used, as an intermediate value, to store AbsE
  float clusterEta[NMaxClusters];
  float clusterPhi[NMaxClusters];
  int seedCellID[NMaxClusters];
};

struct TopoAutomatonTemporaries
{
  tag_type mergeTable[NMaxClusters];
  float seedCellPhi[NMaxClusters];
  int continueFlag;
};


struct TopoAutomatonOptions
{
  float seed_threshold, grow_threshold, terminal_threshold;
  bool abs_seed, abs_grow, abs_terminal;
  int validSamplingSeed;
  //This is used to pack the bits that tell us whether a sample can be used to have seeds or not.
};
//Just a bundled way to pass the options for the Topo-Automaton Clustering.


#endif //CALORECGPU_CUDAFRIENDLYCLASSES_H