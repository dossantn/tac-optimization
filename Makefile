COMPILER = nvcc

OLEVEL = -O2

NUMBER = 10

EXTRAFLAGS = -arch=sm_50 -rdc=true -lcudadevrt -expt-relaxed-constexpr -default-stream per-thread

FOLDER1 = ~/storage/Results/samenoise/run_ttbar/default_validation
FOLDER2 = ~/storage/Results/samenoise/run_jets/default_validation

all: optim

test: optim
	./optim $(FOLDER1) 10 "ttbar" 1

optim: Optimize.cu
	$(COMPILER) Optimize.cu -o optim $(EXTRAFLAGS) $(OLEVEL)

Optimize.cu: CUDAFriendlyClasses.h DataHolders.h Helpers.h EventProcessing.h
	touch $@

CUDAFriendlyClasses.h DataHolders.h Helpers.h EventProcessing.h:
	touch $@

clean:
	rm -f optim
	rm -f *~

debugver: Optimize.cu
	nvcc Optimize.cu -o optim -arch=sm_50 -rdc=true -lcudadevrt -expt-relaxed-constexpr -default-stream per-thread -G -Xcompiler -rdynamic


debugtest: debugver
	cuda-memcheck ./optim $(FOLDER1) 10 "ttbar" 1
