// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

#define TTAC_CALCULATE_PHI_BY_SIMPLE_AVERAGE 1

#include "Helpers.h"
#include "TestDefinitions.h"

#include "DefaultImplementation.h"
#include "HalvedPairs.h"
#include "FixedPairs.h"

void * CUDA_Helpers::allocate(const size_t num)
{
  void * ret;
  CUDA_ERRCHECK(cudaMalloc(&ret, num));
  return ret;
}

void CUDA_Helpers::deallocate(void * address)
{
  CUDA_ERRCHECK(cudaFree(address));
}

void CUDA_Helpers::GPU_to_CPU(void * dest, const void * const source, const size_t num)
{
  CUDA_ERRCHECK(cudaMemcpy(dest, source, num, cudaMemcpyDeviceToHost));
}

void CUDA_Helpers::CPU_to_GPU(void * dest, const void * const source, const size_t num)
{
  CUDA_ERRCHECK(cudaMemcpy(dest, source, num, cudaMemcpyHostToDevice));
}

void CUDA_Helpers::GPU_to_GPU(void * dest, const void * const source, const size_t num)
{
  CUDA_ERRCHECK(cudaMemcpy(dest, source, num, cudaMemcpyDeviceToDevice));
}

int main(int argc, char ** argv)
{
  std::string folder = ".";
  int max_events = 32,
      num_reps = test_function::s_num_reps,
      compress_loops_after = test_function::s_compress_loops_after,
      keep_best = test_function::s_keep_best;
  std::string prefix = test_function::s_prefix;

  if (argc > 1)
    {
      folder = argv[1];
    }
  if (argc > 2)
    {
      max_events = std::atoi(argv[2]);
    }
  if (argc > 3)
    {
      prefix = argv[3];
    }
  if (argc > 4)
    {
      num_reps = std::atoi(argv[4]);
    }
  if (argc > 5)
    {
      compress_loops_after = std::atoi(argv[5]);
    }
  if (argc > 6)
    {
      keep_best = std::atoi(argv[6]);
    }

  TestHolder tests(folder, max_events);

  test_function::s_num_reps = num_reps;
  test_function::s_compress_loops_after = compress_loops_after;
  test_function::s_keep_best = keep_best;
  test_function::s_prefix = prefix;

  loop_range threads{1, 8, 1};
  loop_range blocks{32, 1024, 32};
  //loop_range blocks{128, 1024, 128};

  //tests.add_snr_func("default_snr", DefaultImplementation::signal_to_noise, threads, blocks);
  //tests.add_pair_func("default_pairs", DefaultImplementation::cell_pairs, threads, blocks);
  //tests.add_pair_func("half_pairs", HalvedPairs::cell_pairs, threads, blocks);
  tests.add_grow_func("default_growing", DefaultImplementation::cluster_growing, threads, blocks, blocks, blocks, blocks);
  tests.add_grow_func("half_pair_growing", HalvedPairs::cluster_growing, threads, blocks, blocks, blocks);
  tests.add_grow_func("fixed_pair_growing", FixedPairs::cluster_growing, threads, blocks, blocks, blocks);
  //tests.add_finalize_func("default_finalize", DefaultImplementation::finalize_clusters, threads, blocks, blocks);

  tests.do_tests();

  return 0;
}
